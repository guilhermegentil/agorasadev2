AgoraSADEv2
===========

Segunda versão do prototipo usando a biblioteca GeoEXT 1.1 com ExtJS 3.4 e OpenLayers 2.10

OBS:

1) Para rodar adequadamente o prototipo é necesário possuir:

	a - Python 2.7
		Windows: Instalado em C:\Python27
		Linux: Posuir python no PATH
		
2) Fazer o deploy da aplicação em formato .war no servidor Tomcat(6 ou 7)
	
	a - compactar em formato ZIP todos os arquivos em um arquivo chamado
	    AgoraSADEv2.war
