OpenLayers.ProxyHost = "/AgoraSADEv2/cgi-bin/proxy.cgi?url=";

Ext.chart.Chart.CHART_URL = '/AgoraSADEv2/ext/resources/charts.swf';


/****PARA DEBUG*****/
var DEBUG_RISK; // se undefined = debug desativado
var DEBUG_TENDENCY;
/*******************/

//intervalos de visualizacao dos graficos
var SHORT_GRAPH_TIME_PERIOD = 1; //1 hora
var LONG_GRAPH_TIME_PERIOD = 8; //12 horas
var OBSERVED_PROPERTY_WATER_LEVEL = 'GAUGE_HEIGHT';

//intervalo de atualizacao dos dados
var UPDATE_INTERVAL = 60000; //1 min

var SOS_SERVICES = ["http://lasdpc2.icmc.usp.br:9145/52nSOSv3/sos?"];

var RISK_TABLE = {
    veryLow: 0.0,
    low: 600,
    moderate: 800,
    high: 1000,
    veryHigh: 1200,
    count: 4		  
}

var GRAPH_SIZE = {
    small: 5,
    medium: 10,
    large: 15
}

var DEFAULT_POINT_VECTOR_STYLE = new OpenLayers.Style({
    'pointRadius': 6,
    'fillColor': '#F80000',
    'label': "${name}",
    'fontSize': "12px",
    'fontFamily': "Courier New, monospace",
    'fontWeight': "bold",
    'labelAlign': "rb",
    'labelXOffset': "-10",            
    'labelOutlineColor': "white",
    'labelOutlineWidth': "3"
});
		
var SELECTED_POINT_VECTOR_STYLE = new OpenLayers.Style({
    'pointRadius': 8,
    'fillColor': '#22e988',
    'label': "${name}",
    'fontSize': "12px",
    'fontFamily': "Courier New, monospace",
    'fontWeight': "bold",
    'labelAlign': "rb",
    'labelXOffset': "-10",            
    'labelOutlineColor': "white",
    'labelOutlineWidth': "3"
}); 
