/**
 * Classe responsavel por armazenar as informacoes do servico SOS e 
 * realizar o carregamento inicial dos dados
 */

Sensor_Service = OpenLayers.Class({
    url:null, /* URL de acesso ao servico */
    map: null, /* Mapa onde o VectorMap sera adicionado */
    capsformat: new OpenLayers.Format.SOSCapabilities(), /* define o parser para a resposta da requisicao GetCapabilities */
    SOSCapabilities: null, /* Armazena as capacidades do sensor */
    serviceLayer: null, /* Layer com as posicoes dos sensores */
    defaultProjection: new OpenLayers.Projection("EPSG:4326"), /* Tipo de projeção dos dados, no caso WGS84 */
    offeringCount: 0, /* Quantidade de dados (Ex: Nivel de agua, Temp, Pressão, etc..) oferecida pelo servico */
    serviceName: "", /* Nome do servico */
    foiIds: [], /* Codigos dos sensores que formam esse servico */
	
    /**
	 * Construtor
	 */
    initialize: function(options){
	
        /**
         * Carrega os parametros serviceURLs e map
         */
        OpenLayers.Util.extend(this, options);
        
		/**
		 * Parametros para a requisicao GET inicial
		 *
		 * O parametro 'GetCapabilities' solicita informacoes sobre as capacidades e 
		 * dados fornecidos pelo servico
		 */
        var params = {
            'service': 'SOS', 
            'request': 'GetCapabilities'
        }; 

		/**
		 * URL para a chamada get
		 */
        var tmpUrl = OpenLayers.Util.urlAppend(this.url, OpenLayers.Util.getParameterString(params));
        
		/**
		 * Execita a chamada GET
		 *
		 * OBS: Para que as requisicoes GET funcionem corretamente, a aplicacao 
		 * deve estar sendo servida por um servidor web. Isso e necessario pois as requisicoes
		 * sao desviadas via proxy.
		 *
		 * ver: ..\WEB-INF\cgi-bin\proxy.cgi
		 */
        OpenLayers.Request.GET({
            url: tmpUrl,
            success: this.parseSOSCaps,
            failure: function(response){
				alert("Falha na requisicao GET para o servidor SOS!");
			},
			scope: this,
			async: false
        });
		
    },
    parseSOSCaps: function(response) {
        var styleMap = new OpenLayers.StyleMap({
			'default': DEFAULT_POINT_VECTOR_STYLE,
			'select': SELECTED_POINT_VECTOR_STYLE
		});
        
        /**
         * armazena o SOSCapabilities com as informacoes do servico
         */
        this.SOSCapabilities = this.capsformat.read(response.responseXML || 
                response.responseText);
        
        /**
         * Define o nome do servico
         */
        this.serviceName = this.SOSCapabilities.serviceProvider.providerName;
        
        /**
         * cria vector layer com as posicoes dos sensores
         */    
		this.foiIds = this.getFois();
		 
		 /**
		  * Monta uma camada do tipo vector do OpenLayers para armazenar os dados dos 
		  * sensores, inclusive a posicao geografica
		  */
        this.serviceLayer = new OpenLayers.Layer.Vector(this.serviceName, {
            strategies: [new OpenLayers.Strategy.Fixed()],
            protocol: new OpenLayers.Protocol.SOS({
                formatOptions: {
                    internalProjection: this.map.getProjectionObject(), 
                    externalProjection: this.defaultProjection
                },
                url: this.url,
                fois: this.foiIds //fois: features of interest
            }),
            styleMap: styleMap
        }); 
        
        this.map.addLayer(this.serviceLayer);
    },
    getFois: function() {
        var result = [];
        
        for (var name in this.SOSCapabilities.contents.offeringList) {
            var offering = this.SOSCapabilities.contents.offeringList[name];
            this.offeringCount++;
            for (var i=0, len=offering.featureOfInterestIds.length; i<len; i++) {
                var foi = offering.featureOfInterestIds[i]
                if (OpenLayers.Util.indexOf(result, foi) === -1) {
                    result.push(foi);
                }
            }
        }
        return result;
    }		
});

/**
 * Classe criada com o intuito de gerenciar varios servisos SOS simultaneamente,
 * entretanto, ate o momento apenas funciona para uma unica URL de servico
 */
SOS_Client = OpenLayers.Class({
    serviceURLs: new Array(), /* Array de urls contendo todos os servicos */
    map: null,
    services: [],
	
    initialize: function(options){
	
        /**
         * Carrega os parametros serviceURLs e map
         */
        OpenLayers.Util.extend(this, options);
                
        for(i = 0; i < this.serviceURLs.length; i++){
            this.services.push(new Sensor_Service({
                url: this.serviceURLs[i],
                map: this.map
            }));
        }                        
    }	
});
