/**
 * Copyright (C) 2012  Guilherme Gentil do Amaral
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 */

/** 
 * NOTE: This software is based on the examples provided by  
 * The Open Source Geospatial Foundation
 */

/*Por motivos de seguranÃƒÂ§a aplicativos javascript nao conseguem acesar dados fora
 *do dominio da propria aplicacao, ou seja, um aplicativo rodando em http://localhost:8080 
 *nÃƒÂ£o consegue acessar um arquivo localizado em http://192.168.123.123:5678 
 *para solucionar o problema utiliza-se um script de redirecionamento*/
 
// Add the additional 'advanced' VTypes
Ext.apply(Ext.form.VTypes, {
    daterange : function(val, field) {
        var date = field.parseDate(val);

        if(!date){
            return false;
        }
        if (field.startDateField) {
            var start = Ext.getCmp(field.startDateField);
            if (!start.maxValue || (date.getTime() != start.maxValue.getTime())) {
                start.setMaxValue(date);
                start.validate();
            }
        }
        else if (field.endDateField) {
            var end = Ext.getCmp(field.endDateField);
            if (!end.minValue || (date.getTime() != end.minValue.getTime())) {
                end.setMinValue(date);
                end.validate();
            }
        }
        /*
         * Always return true since we're only using this vtype to set the
         * min/max allowed values (these are tested for after the vtype test)
         */
        return true;
    }
});

function getObservationByFoi(store, sensorClient, foi, offering, startDate, endDate){
	
	var resultArray = [];   
	var responseFormat;  
	var observedProperty;
	var serviceCapabilities;
	var serviceUrl;
	
	var obsformat = new OpenLayers.Format.SOSGetObservation();
	
	serviceCapabilities = sensorClient.services[0].SOSCapabilities;
	serviceUrl = sensorClient.services[0].url;
	
	if (!responseFormat) { // verifica se e undefined
		for (var format in serviceCapabilities.operationsMetadata.GetObservation.parameters.responseFormat.allowedValues) {                
			if (format.indexOf('text/xml') >= 0) {
				responseFormat = format;
			}
		}
	}
	
	if (!observedProperty) { // verifica se e undefined
		for (var property in serviceCapabilities.operationsMetadata.GetObservation.parameters.observedProperty.allowedValues) {                
			if (property.indexOf('waterlevel') >= 0) {
				observedProperty = property;
			}
		}
	}			
	
	var params = {
		'SERVICE': 'SOS', 
		'REQUEST': 'GetObservation',
		'OFFERING': offering,
		'OBSERVEDPROPERTY': observedProperty,
		'VERSION': serviceCapabilities.version,
		'RESPONSEFORMAT': responseFormat,
		
		'RESULTMODEL' : 'om:Measurement',
		'RESPONSEMODE' : 'inline',
		
		'FEATUREOFINTEREST': foi,
		'EVENTTIME' : startDate.format("yyyy-mm-dd'T'HH:MM:ss-0300") + '/' + endDate.format("yyyy-mm-dd'T'HH:MM:ss-0300")
	};
	
	var tmpUrl = OpenLayers.Util.urlAppend(serviceUrl, OpenLayers.Util.getParameterString(params));	
		
	OpenLayers.Request.GET({
		url: tmpUrl,
		success: function(response){
			var output = obsformat.read(response.responseXML || response.responseText); //Faz parse no XML de resposta para obter Measurement
			
			//Organiza em ordem crescente
			output.measurements.sort(function(a, b) {
				return (a.samplingTime.timeInstant.timePosition > b.samplingTime.timeInstant.timePosition)? 1: -1;
			});
			
			if (output.measurements.length > 0) {							
				
				for (var i = 0; i < output.measurements.length; i++) {

					var dt = new Date(output.measurements[i].samplingTime.timeInstant.timePosition);			
					//dt.setHours(dt.getHours() - 1);										
				
					var outputData = [
						output.measurements[i].observedProperty.substr(34),
						//formatedDate +" - "+ formatedTime,	
						//output.measurements[i].samplingTime.timeInstant.timePosition,		
						dt.format("dd/mm/yyyy-HH:MM:ss"),
						output.measurements[i].result.value,
						output.measurements[i].result.uom
					];                            
					resultArray.push(outputData);
				}                                                                
				store.loadData(resultArray);
			}
		},
		failure: function(response){
			alert("Falha na requisiÃ§Ã£o GET para o SOS");
		},
		scope: this,
		async: false
	});		
}

function onFeatureSelect(feature, offering, sensorClient, sosStore, startDate, endDate) {                                                 

	if(offering == 'GAUGE_HEIGHT'){
		getObservationByFoi(sosStore, sensorClient, feature.attributes.id, offering, startDate, endDate);
	}
	  
}

function maskPage(mask){
	if(mask)
		Ext.getBody().mask('Carregando...', 'x-mask-loading');
	else
		Ext.getBody().unmask();
}

function getRisk(rStore, tStore, storeSensor1, storeSensor2){

	var value = 0;
	var tendency = 0;	
	var tendencyInf = 0;
	var tendencySup = 0;
	var rArray = [];
	var tArray = [];

	//Seleciona o limite do menor vetor para evitar acesso fora do vetor (nullPointerException)
	var length = storeSensor1.data.length < storeSensor2.data.length ? storeSensor1.data.length : storeSensor2.data.length;			
	
	var startPosition = (RISK_TABLE.count%2 == 0) ? (length - RISK_TABLE.count) : (length - RISK_TABLE.count - 1);
	
	var middle = startPosition + ((length - startPosition) / 2);
	
	//Calcula a media das (RISK_TABLE.count) ultimas medicoes dos dois sensores 
	for(i = startPosition; i < length; i++){ 
		//calcula o risco
		if (typeof storeSensor1.data.items[i] != 'undefined') {
			var avg = (parseFloat(storeSensor1.data.items[i].data.Valor) + parseFloat(storeSensor2.data.items[i].data.Valor)) / 2;		
		}
		else avg = 0;
		
		//calcula a tendencia 
		if(i < middle)
			tendencyInf += avg;
		else
			tendencySup += avg;
			
		value += avg
	}
	
	value /= RISK_TABLE.count;
	
	//Calcula o risco e monta array
	if(DEBUG_RISK) //Para DEBUG
		value = DEBUG_RISK;			
	
	if(value < RISK_TABLE.low){
		rArray.push(['Muito baixo', value, 0, 0, 0, 0]);
	}
	else if(value < RISK_TABLE.moderate){
		rArray.push(['Baixo', RISK_TABLE.veryLow, value, 0, 0, 0]);
	}
	else if(value < RISK_TABLE.high){
		rArray.push(['Moderado', RISK_TABLE.veryLow, RISK_TABLE.low, value, 0, 0]);
	}
	else if(value < RISK_TABLE.veryHigh){
		rArray.push(['Alto', RISK_TABLE.veryLow, RISK_TABLE.low, RISK_TABLE.moderate, value, 0]);
	}
	else{ //very high
		rArray.push(['Muito alto', RISK_TABLE.veryLow, RISK_TABLE.low, RISK_TABLE.moderate, RISK_TABLE.high, value]);
	}
	
	//Calcula tendencia e monta array
	tendency = (tendencySup - tendencyInf) / RISK_TABLE.count;
	
	//DEBUG
	if(DEBUG_TENDENCY)
		tendency = DEBUG_TENDENCY;
	
	var t;
	
	if(tendency < 0){ //Mantem igual ou tendencia indefinida
		t = 'BAIXA';		
	}
	else if(tendency > 0){ //Tendencia de ALTA
		t = 'ALTA';
	}
	else{ //Tendencia de BAIXA
		t = 'INDEFINIDA';
	}
	
	tArray.push([t, tendency]);
	
	//Carrega dados nos stores	
	rStore.loadData(rArray);	
	tStore.loadData(tArray);	
}

function getObservationGrid(store){
	var observationGrid = new Ext.grid.GridPanel({		
		store: store,		
        stripeRows: true, 		
        columns:
		[{
			id       :'Tipo',
			header   : 'Tipo',                      
			sortable : true, 			
			dataIndex: 'Tipo',
        },
		{
			header   : 'Horario', 			
			sortable : true,                 
			dataIndex: 'Horario'
		},
		{
			header   : 'Valor', 			
			sortable : true,                
			dataIndex: 'Valor'
		},
		{
			header   : 'Unidade', 			
			sortable : true,                
			dataIndex: 'Unidade'
		}]  
	})

	return observationGrid;
}

function update(sensorClient, sosStore, feature, storeSensor1, storeSensor2){	
	getObservationByFoi(storeSensor1, sensorClient, 'foi_3001', OBSERVED_PROPERTY_WATER_LEVEL, new Date(new Date() - (LONG_GRAPH_TIME_PERIOD*60*60000)), new Date()); 
	getObservationByFoi(storeSensor2, sensorClient, 'foi_4001', OBSERVED_PROPERTY_WATER_LEVEL, new Date(new Date() - (LONG_GRAPH_TIME_PERIOD*60*60000)), new Date()); 	
	
	if(feature)
		getObservationByFoi(sosStore, sensorClient, feature.attributes.id, OBSERVED_PROPERTY_WATER_LEVEL, new Date(new Date() - (SHORT_GRAPH_TIME_PERIOD*60*60000)), new Date()); 		
}

Ext.onReady(function() {	 
		
	maskPage(true);
		
	//Dados obtidos pelo SOS
	var sosStore = new Ext.data.ArrayStore({
		fields: ['Tipo', 'Horario', 'Valor', 'Unidade']			
	});

	var sosHistoryStore = new Ext.data.ArrayStore({
		fields: ['Tipo', 'Horario', 'Valor', 'Unidade']			
	});	
	
	//Dados das ultimas 24h do sensor 1
	var storeSensor1 = new Ext.data.ArrayStore({
		fields: ['Tipo', 'Horario', 'Valor', 'Unidade']			
	});				
	
	//Dados das ultimas 24h do sensor 2
	var storeSensor2 = new Ext.data.ArrayStore({
		fields: ['Tipo', 'Horario', 'Valor', 'Unidade']			
	});				
	
	var riskStore  = new Ext.data.ArrayStore({
        fields: ['risk', 'veryLow', 'low', 'moderate', 'high', 'veryHigh']        
    });	  

	var tendencyStore  = new Ext.data.ArrayStore({
        fields: ['tendency', 'value']        
    });	
       
    var osmBaseLayer    = new OpenLayers.Layer.OSM("Open Street Map"); 
	var googlePhyMap    = new OpenLayers.Layer.Google("Google Physical", {type: google.maps.MapTypeId.TERRAIN, numZoomLevels: 16});
	var googleStreetMap = new OpenLayers.Layer.Google("Google Streets", {numZoomLevels: 18});
    var googleHybMap    = new OpenLayers.Layer.Google("Google Hybrid", {type: google.maps.MapTypeId.HYBRID, numZoomLevels: 18});
    var googleSatMap    = new OpenLayers.Layer.Google("Google Satellite", {type: google.maps.MapTypeId.SATELLITE, numZoomLevels: 18});
               
    var sensorClient, wmsFeature, viewport, map, mapPanel, selectedFeature;  
       
    /*Cria um mapa com controles de navegacao*/
    map = new OpenLayers.Map({        
        controls:[
			new OpenLayers.Control.LayerSwitcher(),
			new OpenLayers.Control.MousePosition(),
			new OpenLayers.Control.PanZoomBar(),
			new OpenLayers.Control.Navigation()
        ],                           
        maxExtent: new OpenLayers.Bounds(-20037508.34,-20037508.34,20037508.34,20037508.34),
        maxResolution: 156543.0399,       
        zoom: 14,
        units: 'm',
        projection: new OpenLayers.Projection("EPSG:900913"),      //Spherical Mercator
        displayProjection: new OpenLayers.Projection("EPSG:4326"), //WGS84
        layers: [osmBaseLayer, googlePhyMap, googleStreetMap, googleSatMap, googleHybMap]
    });                     			
	
    mapPanel = new GeoExt.MapPanel({            
        map: map,                   
        /*Centraliza em Sao Carlos*/
        center: new OpenLayers.LonLat(-47.89807,-22.00578).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject()),		
    });  

	sensorClient = new SOS_Client(
    {
        serviceURLs: SOS_SERVICES,
        map: map
    });	
	
	 /*
	 * carrega as camadas dos sensores e adiciona controle para selecao
	 */
	var layers = [];
	for(i = 0; i < sensorClient.services.length; i++)
	{
		if(sensorClient.services[i].serviceLayer != null)
			layers.push(sensorClient.services[i].serviceLayer);
	}
	
	getObservationByFoi(storeSensor1, sensorClient, 'foi_3001', OBSERVED_PROPERTY_WATER_LEVEL, new Date(new Date() - (LONG_GRAPH_TIME_PERIOD*60*60000)), new Date()); 
	getObservationByFoi(storeSensor2, sensorClient, 'foi_4001', OBSERVED_PROPERTY_WATER_LEVEL, new Date(new Date() - (LONG_GRAPH_TIME_PERIOD*60*60000)), new Date()); 

	
	/*
	 * adiciona controle para selecao dos sensores
	 */  	 
	var selectControl = new OpenLayers.Control.SelectFeature(layers,
	{			
		scope: this, 
		onSelect: function(feature){
			selectedFeature = feature;
			maskPage(true);						
			onFeatureSelect(selectedFeature, OBSERVED_PROPERTY_WATER_LEVEL, sensorClient, sosStore, new Date(new Date() - (SHORT_GRAPH_TIME_PERIOD*60*60000)), new Date()); 
			maskPage(false);			
		},                                                    
		onBeforeSelect: function(){			
			sosStore.removeAll(); // Apaga os dados 
		},
		onUnselect: function(){			
			sosStore.removeAll(); // Apaga os dados 
		},
		autoActivate: true
	});	
	
	map.addControl(selectControl);
	
	/*
     *Constroi arvore de camadas     
     */
    var layerRoot = new Ext.tree.TreeNode({
        text: "All Layers"
    });
    
    layerRoot.appendChild(new GeoExt.tree.BaseLayerContainer({
        text: "Mapa Base",
        map: map,
        expanded: true
    }));
   
    var layerTree = new Ext.tree.TreePanel({
        title: "Mapa Base",
        root: layerRoot,
        rootVisible: false, 
        autoScroll: true,
        listeners: {             
            checkchange: function(node, checked) {    
                if (node.attributes.layer.isBaseLayer == false){
                    if (checked === true) {                                                        
                        mapPanel.map.addLayer(node.attributes.layer);                                        
                    } else {                                           
                        mapPanel.map.removeLayer(node.attributes.layer);                                  
                    }
                }
            }            
        }        
    });
	
	/*
	* Definicao do risco media das ultimas N medicoes
	*/			
	getRisk(riskStore, tendencyStore, storeSensor1, storeSensor2);
    
	/*
     *Layout geral da aplicacao
     **/		
    viewport = new Ext.Viewport({
        layout: 'border', 		
        items: [{ 
			xtype: "panel",
			title: "Gr&aacute;ficos e imagens",
			ref: "east",
			region: "east",				
			collapsible: false,
			width: 500,
			layout: {
				type: "vbox",				
				align: "stretch"			
			},
			items: [
			{
				xtype: "panel",				
				title: "USP - Ultimas " + LONG_GRAPH_TIME_PERIOD + " horas",
				ref: "graph1Panel",
				flex: 1,
				layout: 'fit',
				items: [{
					xtype: 'linechart',
					store: storeSensor1,
					xField: 'Horario',					
					xAxis: new Ext.chart.CategoryAxis({
						title: 'Hora'
					}),
					yAxis: new Ext.chart.NumericAxis({
						title: 'Nivel(cm)',
						minimum: 0,
						maximum: 300,
						majorUnit: 50
					}),
					series:[{
						yField: 'Valor',
						style: {
							size: GRAPH_SIZE.small						
						}
					}],
					chartStyle: {                
						animationEnabled: false
					},
					extraStyle: {
						yAxis: {
							titleRotation: -90,							
							hideOverlappingLabels: true
						}
					}
				}]
			},
			{
				xtype: "panel",
				title: "KARTODROMO - Ultimas " + LONG_GRAPH_TIME_PERIOD + " horas",
				ref: "graph2Panel",
				flex: 1,
				layout: 'fit',
				items: [{
					xtype: 'linechart',
					store: storeSensor2,
					xField: 'Horario',					
					xAxis: new Ext.chart.CategoryAxis({
						title: 'Hora'
					}),
					yAxis: new Ext.chart.NumericAxis({
						title: 'Nivel(cm)',
						minimum: 0,
						maximum: 300,
						majorUnit: 50
					}),
					series:[{
						yField: 'Valor',						
						style: {							
							size: GRAPH_SIZE.small	
						}
					}],
					chartStyle: {                
						animationEnabled: false
					},
					extraStyle: {
						yAxis: {
							titleRotation: -90,
							hideOverlappingLabels: true
						}
					}
				}]	
			},
			{
				flex: 1,
				xtype: 'panel',
				layout: 'fit',
				items: [{
					xtype: "form",
					title: "Dados Hist&oacute;ricos",				
					ref: "form1Panel",					
					defaults: {width: 350},
					frame:true,        
					bodyStyle:'padding:5px 5px 0',					
					defaultType: 'datefield',					
					items: [{
						xtype: 'combo',
						id: 'foiCmb',
						fieldLabel: 'Sensor',
						hiddenName: 'ddi_sensor',
						emptyText: 'Selecione o sensor...',
						store: 
						  new Ext.data.SimpleStore({
							fields: ['foi','sensor'],
							data: [
							  ["foi_3001","USP"],["foi_4001","KARTODROMO"]
								  ]
						}), // end of Ext.data.SimpleStore
						displayField: 'sensor',
						valueField: 'foi',
						selectOnFocus: true,
						mode: 'local',
						typeAhead: true,
						editable: false,
						triggerAction: 'all',						
					},{						
						fieldLabel: 'Data inicial',
						editable: false,
						name: 'startdt',
						id: 'startdt',
						vtype: 'daterange',
						format: 'd/m/Y',
						endDateField: 'enddt' // id of the end date field
					},{						
						fieldLabel: 'Data final',
						editable: false,
						name: 'enddt',
						id: 'enddt',
						vtype: 'daterange',
						format: 'd/m/Y',
						startDateField: 'startdt' // id of the start date field
					}],
					buttons: [{
						text: 'Buscar',
						handler: function(){
						
							function parseDt(dt){
								var d = dt.substr(0, 2);
								var m = dt.substr(3, 2);
								var y = dt.substr(6, 4);
								
								return m +'/'+ d +'/'+ y;
							}											
							
							var startDate = new Date(parseDt(Ext.getCmp('startdt').value));												
							var endDate = new Date(parseDt(Ext.getCmp('enddt').value));
							
							startDate.setDate(startDate.getDate());
							endDate.setDate(endDate.getDate()+1);

							var selectedFoi = Ext.getCmp('foiCmb').value;							
							
							maskPage(true);	
							getObservationByFoi(sosHistoryStore, sensorClient, selectedFoi, OBSERVED_PROPERTY_WATER_LEVEL, startDate, endDate); 
							maskPage(false);	
						
							var win = new Ext.Window({								
								layout:'fit',
								width:840,
								height:480,
								closable : false,
								//plain: true,
								title: 'Dados Hist&oacute;ricos',								

								items: [{
									xtype: 'tabpanel',
									activeTab: 1,
									items: [{
										title: 'Tabela',
										layout: 'fit',
										items: [getObservationGrid(sosHistoryStore)]
									},{
										title: 'Grafico',
										layout: 'fit',
										items: [{
											xtype: 'panel',
											layout: 'fit',
											items: [{
												xtype: 'linechart',
												id: 'selectedHistorySensorChart',
												store: sosHistoryStore,
												xField: 'Horario',
												yField: 'Valor',
												xAxis: new Ext.chart.CategoryAxis({
													title: 'Hora'
												}),
												yAxis: new Ext.chart.NumericAxis({
													title: 'Nivel(cm)'
												}),
												chartStyle: {                
													animationEnabled: false
												},
												extraStyle: {
													yAxis: {
														titleRotation: -90,
														hideOverlappingLabels: true
													}
												}								
											}]					
										}]
									}]
								}],

								buttons: [{
									text: 'Fechar',
									handler: function(){
										sosHistoryStore.removeAll();
										win.destroy();
									}
								}]
							});							
							win.show(this);
						}
					}]
				}]				
			}]
        },    
        {
            region: 'west',
			collapsible: false,
            title: 'Controles',                
            width: 300,
			layout: {
				type: "vbox",				
				align: "stretch"
			},
			items: [{
				flex: 1,
				xtype: "panel",
				title: "Camadas",				
				layout: "fit",
				items: [{
					layout: {
						type: 'accordion',
						animate: true					
					},
					items: [
						layerTree,
						{
							title: "Opcao 1"
						},
						{
							title: "Opcao 2"
						}
					]
				}]								
			},
			{
				flex: 1,
				xtype: "panel",
				title: "An&aacute;lise de risco",
				layout: {				
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					flex: 1,
					xtype: 'panel',
					items: [{
						xtype: 'barchart',
						store: riskStore,
						yField: 'risk',
						yAxis: new Ext.chart.CategoryAxis({
							title: 'Risco'
						}),
						xAxis: new Ext.chart.NumericAxis({
							title: 'Nivel(cm)'
						}),					
						series: [{
							xField: 'veryHigh',
							displayName: 'Muito-Alto',
							style:{
								color: '#FE231F'
							}
						},{
							xField: 'high',
							displayName: 'Alto',
							style:{
								color: '#F39A1E'
							}
						},{
							xField: 'moderate',
							displayName: 'Moderado',
							style:{
								color: '#E4E629'
							}
						},{
							xField: 'low',
							displayName: 'Baixo',
							style:{
								color: '#AFE429'
							}
						},{
							xField: 'veryLow',
							displayName: 'Muito-Baixo',
							style:{
								color: '#76D90C'
							}
						}],
						chartStyle: {                
							animationEnabled: false
						},
						extraStyle: {
							yAxis: {
								titleRotation: -90,
								labelRotation: -90
							}
						}
					}]
				},{
					flex: 1,
					xtype: 'panel',
					title: 'Tend&ecirc;ncia de nivel',
					items: [{
						xtype: 'columnchart',
						store: tendencyStore,
						xField: 'tendency',
						xAxis: new Ext.chart.CategoryAxis({
							title: 'Tendencia'
						}),
						yAxis: new Ext.chart.NumericAxis({
							title: 'Nivel(cm)'
						}),					
						series: [{
							yField: 'value',
							displayName: 'Tendencia',
							style:{
								color: '#76D90C'
							}
						}],
						chartStyle: {                
							animationEnabled: false
						},
						extraStyle: {
							yAxis: {
								titleRotation: -90							
							}
						}
					}]
				}]
			}]	            			       
        },
		{
            region: "center", 						
            layout: {
                type: "vbox",								
				align: "stretch"
            },
            items: [{
				xtype: "panel",
				title: "Mapa",
				flex: 1,
				layout: "fit",
				items: mapPanel
			},{
				xtype: "panel",
				title: "Medi&ccedil;&otilde;es recentes - &Uacute;ltimas " + SHORT_GRAPH_TIME_PERIOD + " horas",
				flex: 0.48,
				layout: "fit",
				items: [{
					xtype: "tabpanel",				
					activeTab: 1,																			
					items: [{
						title: 'Tabela',
						layout: 'fit',
						items: [getObservationGrid(sosStore)]
					},{
						title : 'Grafico',
						layout: 'fit',
						items: [{
							xtype: 'panel',
							layout: 'fit',
							items: [{
								xtype: 'linechart',
								id: 'selectedSensorChart',
								store: sosStore,
								xField: 'Horario',
								yField: 'Valor',
								xAxis: new Ext.chart.CategoryAxis({
									title: 'Hora'
								}),
								yAxis: new Ext.chart.NumericAxis({
									title: 'Nivel(cm)'
								}),
								chartStyle: {                
									animationEnabled: false
								},
								extraStyle: {
									yAxis: {
										titleRotation: -90,
										hideOverlappingLabels: true
									}
								}								
							}]					
						}]
					}]
				}]								
			}]         
        }]
    });	
	
	maskPage(false); 

	window.setInterval(function(){
		getRisk(riskStore, tendencyStore, storeSensor1, storeSensor2);
		update(sensorClient, sosStore, selectedFeature, storeSensor1, storeSensor2);
	}, UPDATE_INTERVAL);
});