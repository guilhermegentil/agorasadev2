/**
 * Por motivos de seguranca aplicativos javascript nao conseguem 
 * acesar dados fora do dominio da propria aplicacao, ou seja, 
 * um aplicativo rodando em http://localhost:8080 nao consegue 
 * acessar um arquivo localizado em http://192.168.123.123:5678 para 
 * solucionar o problema utiliza-se um script de redirecionamento
 */

/**
 * Estrutura para validar as datas maximas e minimas
 * para seleção de dados historicos 
 */
Ext.apply(Ext.form.VTypes, {
    daterange : function(val, field) {
        var date = field.parseDate(val);

        if(!date){
            return false;
        }

        /**
         * Verifica se a data inicial foi definida e define o seu valor maximo
         */
        if (field.startDateField) {
            var start = Ext.getCmp(field.startDateField);
            if (!start.maxValue ||
                (date.getTime() != start.maxValue.getTime())) {
                    start.setMaxValue(date);
                    start.validate();
            }
        }

        /**
         * Verifica se a data final foi definida e define o seu valor minimo
         */
        else if (field.endDateField) {
            var end = Ext.getCmp(field.endDateField);
            if (!end.minValue || 
                (date.getTime() != end.minValue.getTime())) {
                    end.setMinValue(date);
                    end.validate();
            }
        }
		
        /**
         * Retorna verdadeiro pois nao é feita validacao nessa etapa.
         */
        return true;
    }
});

/**
 * Funcao responsavel por retornar uma medicao do sensor(observation),
 * dado o codigo de identificacao do sensor. 
 *
 * @param {store} Estrutura de armazenamento do ExtJS. Aramzena dados para posterior
 *     exibicao em listas, graficos, grids, etc...
 * @param {sensorClient} Objeto do tipo SOS_Client. Armazena os enderecos dos servisos SOS
 * @param {foi} Codigo do sensor
 * @param {offering} Codigo no padrao OGC que define o tipo de dado desejado
 * @param {starDate} Data de inicio para busca da medicao por periodo
 * @param {endDate} Data de fim para busca da medicao por periodo
 */
function getObservationByFoi(store, sensorClient, foi, offering, startDate, endDate){
	
    var resultArray = [];   
    var responseFormat;  
    var observedProperty;
    var serviceCapabilities;
    var serviceUrl;	
    var obsformat = new OpenLayers.Format.SOSGetObservation();
	
    serviceCapabilities = sensorClient.services[0].SOSCapabilities;
    serviceUrl = sensorClient.services[0].url;
	
	/**
	 * Caso responseFormat nao estaja definido, faz uma busca nos valores permitidos
     * para a resposta em serviceCapabilities e carrega o responseFormat
	 */
    if (!responseFormat) { 
		for (var format in serviceCapabilities.operationsMetadata.
				GetObservation.parameters.responseFormat.
				allowedValues) {
					if (format.indexOf('text/xml') >= 0) {
						responseFormat = format;
					}
				}
    }
	
	/**
	 * Caso observedProperty nao estaja definido, faz uma busca nos valores permitidos
     * para a medicao em serviceCapabilities e carrega o observedProperty
	 */
    if (!observedProperty) { // verifica se e undefined
    	for (var property in 
                 serviceCapabilities.operationsMetadata.
                 GetObservation.parameters.observedProperty.
                 allowedValues) {                
		    if (property.indexOf('waterlevel') >= 0) {
		        observedProperty = property;
		    }
	}
    }			
	
	/**
	 * Parametros para a requisicao GET ao serviso SOS
	 */
    var params = {
    'SERVICE': 'SOS', 
	'REQUEST': 'GetObservation',
	'OFFERING': offering,
	'OBSERVEDPROPERTY': observedProperty,
	'VERSION': serviceCapabilities.version,
	'RESPONSEFORMAT': responseFormat,
	'RESULTMODEL' : 'om:Measurement',
	'RESPONSEMODE' : 'inline',
	'FEATUREOFINTEREST': foi,
	'EVENTTIME' : startDate.format("yyyy-mm-dd'T'HH:MM:ss-0300") +
                '/' + endDate.format("yyyy-mm-dd'T'HH:MM:ss-0300")
    };
	
	/**
	 * URL para a requisicao GET
	 */
    var tmpUrl = OpenLayers.Util.urlAppend(serviceUrl, 
                 OpenLayers.Util.getParameterString(params));	
	
	/**
	 * Executa requisicao GET ao servico SOS.
	 */
    OpenLayers.Request.GET({
        url: tmpUrl,
		success: function(response){
            var output = obsformat.read(response.responseXML || 
                response.responseText); 
			
	    /* Organiza o resultado em ordem crescente */
	    output.measurements.sort(function(a, b) {
	        return (a.samplingTime.timeInstant.timePosition > 
                    b.samplingTime.timeInstant.timePosition)? 1: -1;
	    });
		
		/* Se a resposta for mair que zero, formata a data */
	    if (output.measurements.length > 0) {		
	        for (var i = 0; i < output.measurements.length; i++) {
		    var dt = new Date(
                        output.measurements[i].samplingTime.
                        timeInstant.timePosition);		    
		    var outputData = [
		        output.measurements[i].
                            observedProperty.substr(34),
                        dt.format("dd/mm/yyyy-HH:MM:ss"),
		        output.measurements[i].result.value,
		        output.measurements[i].result.uom
		    ];
			
			/* Armazena o resultado parcial da resposta */
		    resultArray.push(outputData);

	        }
			/* Armazena a resposta da solicitacao GET */
            store.loadData(resultArray);
	    }
	},
		failure: function(response){
			alert("Falha na requisiÃ§Ã£o GET para o SOS");
        },
	scope: this,
	async: false
    });		
}


/**
 * Funcao responsavel por invocar a funcao getObservationByFoi(...) dado um certo
 * offering
 *
 * @param {feature} Sensor selecionado
 * @param {offering} Codigo no padrao OGC que define o tipo de dado desejado
 * @param {sensorClient} Objeto do tipo SOS_Client. Armazena os enderecos dos servisos SOS
 * @param {sosStore}  Estrutura de armazenamento do ExtJS. Armazenara os dados retornados pelo SOS
 * @param {starDate} Data de inicio para busca da medicao por periodo
 * @param {endDate} Data de fim para busca da medicao por periodo
 */
function onFeatureSelect(feature, offering, sensorClient, sosStore, startDate, endDate){                      
    if(offering == 'GAUGE_HEIGHT'){
        getObservationByFoi(sosStore, sensorClient, 
            feature.attributes.id, offering, startDate, endDate);
    }	  
}

/**
 * Funcao responsavel por carregar uma mascara de 'loading' na tela,
 * para dar ao usuario um feedback dos status de carregamento do sistema
 *
 * @param {mask} Valor Booleano
 */
function maskPage(mask){
    if(mask)
        Ext.getBody().mask('Carregando...', 'x-mask-loading');
    else
        Ext.getBody().unmask();
}

/**
 * Funcao responsavel por calcular o risco dado o nivel das ultimas N medicoes
 *
 * @param {rStore} Estrutura de armazenamento do ExtJS. Armazenara os dados de risco para 
 *     posterior exibicao no grafico de risco
 * @param {tStor} Estrutura de armazenamento do ExtJS. Armazenara os dados de tendencia para 
 *     posterior exibicao no grafico de tendencia
 * @param {storeSensor1} Dados lidos do sensor 1
 * @param {storeSensor2} Dados lidos do sensor 2
 */
function getRisk(rStore, tStore, storeSensor1, storeSensor2){

    var value = 0;
    var tendency = 0;	
    var tendencyInf = 0;
    var tendencySup = 0;
    var rArray = [];
    var tArray = [];

    /**
     * Seleciona o limite do menor vetor para evitar acesso fora do 
     * intervalo
     */
    var length = storeSensor1.data.length < 
            storeSensor2.data.length ? 
            storeSensor1.data.length : storeSensor2.data.length;
	
    var startPosition = (RISK_TABLE.count%2 == 0) ? 
        (length - RISK_TABLE.count) : (length - RISK_TABLE.count - 1);
	
    var middle = startPosition + ((length - startPosition) / 2);

    /**
     * Calcula a media das (RISK_TABLE.count) ultimas 
     * medicoes dos dois sensores
     */
    for(i = startPosition; i < length; i++){     
        // calcula o risco
	    if (typeof storeSensor1.data.items[i] != 'undefined') {
	        var avg = (parseFloat(
                           storeSensor1.data.items[i].data.Valor) + 
                           parseFloat(storeSensor2.data.items[i].
                               data.Valor)) / 2;		
	    }
	    else 
            avg = 0;
		
	    /* calcula a tendencia */
	    if(i < middle)
	        tendencyInf += avg;
	    else
	        tendencySup += avg;
            
	    value += avg
    }
	
    value /= RISK_TABLE.count;
	
    /* Calcula o risco e monta array */
    if(DEBUG_RISK)
        value = DEBUG_RISK;			

    if(value < RISK_TABLE.low){
        rArray.push(['Muito baixo', value, 0, 0, 0, 0]);
    }
    else if(value < RISK_TABLE.moderate){
        rArray.push(['Baixo', RISK_TABLE.veryLow, value, 0, 0, 0]);
    }
    else if(value < RISK_TABLE.high){
        rArray.push(['Moderado', RISK_TABLE.veryLow, 
                RISK_TABLE.low, value, 0, 0]);
    }
    else if(value < RISK_TABLE.veryHigh){
        rArray.push(['Alto', RISK_TABLE.veryLow, 
                RISK_TABLE.low, RISK_TABLE.moderate, value, 0]);
    }
    else{
        rArray.push(['Muito alto', RISK_TABLE.veryLow, 
                RISK_TABLE.low, RISK_TABLE.moderate, 
                RISK_TABLE.high, value]);
    }
	
    /* Calcula tendencia e monta array */
    tendency = (tendencySup - tendencyInf) / RISK_TABLE.count;
	
    /* DEBUG */
    if(DEBUG_TENDENCY)
        tendency = DEBUG_TENDENCY;
	
	/* Armazena o valor da tendencia de nivel */
    var t;
	
    if(tendency < 0){ /* Mantem igual ou tendencia indefinida */
        t = 'BAIXA';		
    }
    else if(tendency > 0){ /* Tendencia de ALTA */
        t = 'ALTA';
    }
    else{ /* Tendencia de BAIXA */
        t = 'INDEFINIDA';
    }
	
    tArray.push([t, tendency]);
	
    /* Carrega dados nos stores	*/
    rStore.loadData(rArray);	
    tStore.loadData(tArray);	
}

/**
 * Funcao que monta um grid de visualizacao dos dados das medicoes dos sensores
 *
 * @param {store} Dados das medicoes
 * @return {observationGrid} Objeto Ext.grid.GridPanel
 */
function getObservationGrid(store){
    var observationGrid = new Ext.grid.GridPanel({		
        store: store,		
        stripeRows: true, 		
        columns:[
        {
            id       :'Tipo',
            header   : 'Tipo',                      
            sortable : true, 			
            dataIndex: 'Tipo',
        },
        {
            header   : 'Horario', 			
            sortable : true,                 
            dataIndex: 'Horario'
	    },
        {
	        header   : 'Valor', 			
            sortable : true,                
            dataIndex: 'Valor'
	    },
        {
            header   : 'Unidade', 			
            sortable : true,                
            dataIndex: 'Unidade'
	    }]  
    })
    return observationGrid;
}

/**
 * Funcao chamada em intervalos de tempo periodicos para atualizar automaticamente os dados
 * das medicoes
 *
 * @param {sensorClient} Objeto do tipo SOS_Client. Armazena os enderecos dos servisos SOS
 * @param {sosStore}  Estrutura de armazenamento do ExtJS. Armazenara os dados retornados pelo SOS
 * @param {feature} Sensor selecionado
 * @param {storeSensor1} Dados lidos do sensor 1
 * @param {storeSensor2} Dados lidos do sensor 2
 */
function update(sensorClient, sosStore, feature, storeSensor1, storeSensor2){

    /**
     * Recupera leitura do sensor 1 e armazena em storeSensor1
     */
    getObservationByFoi(storeSensor1, sensorClient, 'foi_3001',
        OBSERVED_PROPERTY_WATER_LEVEL,
        new Date(new Date() - (LONG_GRAPH_TIME_PERIOD*60*60000)),
        new Date());

    /**
     * Recupera leitura do sensor 2 e armazena em storeSensor2
     */
    getObservationByFoi(storeSensor2, sensorClient, 'foi_4001',
        OBSERVED_PROPERTY_WATER_LEVEL,
        new Date(new Date() - (LONG_GRAPH_TIME_PERIOD*60*60000)),
        new Date());

    /**
     * Recupera leitura do sensor selecionado no mapa e armazena em sosStore
     */
    if(feature)
        getObservationByFoi(sosStore, sensorClient,
            feature.attributes.id, OBSERVED_PROPERTY_WATER_LEVEL,
            new Date(new Date() - (SHORT_GRAPH_TIME_PERIOD*60*60000)),
            new Date());
}

/**
 * Funcao de inicializacao da aplicacao e das bibliotead ExtJS. Esta funcao e chamada quando 
 * ocorre o evento de termino de carregamento da da biblioteca ExtJS. 
 * O Principal objetivo dessa funcao e o carregamento da interface, 
 * montagem do mapa e carregamento dos objetos responsaveis pela 
 * comunicacao com o servico SOS
 *
 */

Ext.onReady(function() {
    /**
     * Adiciona mascara na pagina, efeito de carregamento para exibir a interface
     * somente quando todos os elementos estiverem prontos
     */
    maskPage(true);
    
    /**
     * Objeto ExtJs responsavel pelo armazenamento dos dados dos sensores
     */
    var sosStore = new Ext.data.ArrayStore({
        fields: ['Tipo', 'Horario', 'Valor', 'Unidade']	
    });
    
    /**
     * Objeto ExtJs responsavel pelo armazenamento dos dados historicos dos sensores
     */  
    var sosHistoryStore = new Ext.data.ArrayStore({
        fields: ['Tipo', 'Horario', 'Valor', 'Unidade']
    });	

    /**
     * Dados das ultimas 24h do sensor 1
     */
    var storeSensor1 = new Ext.data.ArrayStore({
        fields: ['Tipo', 'Horario', 'Valor', 'Unidade']
    });				

    /**
     * Dados das ultimas 24h do sensor 2
     */
    var storeSensor2 = new Ext.data.ArrayStore({
        fields: ['Tipo', 'Horario', 'Valor', 'Unidade']	
    });
    
    /**
     * Objeto ExtJs responsavel pelo armazenamento dos dados de risco,
     * o calculo do risco armazena o resultado nesta variavel
     */	
    var riskStore  = new Ext.data.ArrayStore({
        fields: ['risk', 'veryLow', 'low', 'moderate', 'high', 
            'veryHigh']        
    });

    /**
     * Objeto ExtJs responsavel pelo armazenamento dos dados de tendencia de nivel,
     * o calculo da tendencia armazena o resultado nesta variavel
     */	
    var tendencyStore  = new Ext.data.ArrayStore({
        fields: ['tendency', 'value']        
    });	

    /**
     * Mapa base do OpenStreetMaps
     */
    var osmBaseLayer = new OpenLayers.Layer.OSM("Open Street Map"); 
    
    /**
     * Mapas base do Google Maps
     */
	/*
    var googlePhyMap = new OpenLayers.Layer.Google("Google Physical", 
        {type: google.maps.MapTypeId.TERRAIN,numZoomLevels: 16});
    var googleStreetMap = new OpenLayers.Layer.Google("Google Streets",
        {numZoomLevels: 18});
    var googleHybMap = new OpenLayers.Layer.Google("Google Hybrid", 
        {type: google.maps.MapTypeId.HYBRID, numZoomLevels: 18});
    var googleSatMap = new OpenLayers.Layer.Google("Google Satellite",
        {type: google.maps.MapTypeId.SATELLITE, numZoomLevels: 18});
	*/
    /**
     * Principais variaveis do sistema
     */
     
    /* Armazena os enderecos dos servicos SOS utilizados */
    var sensorClient; 
    
    /* Armazena o layout da janela da aplicacao, 
    todos os outros elementos de interface sao definidos dentro dele */
    var viewport; 
    
    /* Objeto OpenLayers responsavel pela exibicao e interacao com o mapa */
    var map; 
    
    /* Objeto GeoExt que encapsula 'map' e facilita a integracao com recursos ExtJS */
    var mapPanel; 
    
    /* Objeto OpenLayers que armazena a 'feature' ou elemento selecionado pelo 
    usuario no mapa */
    var selectedFeature;
       
    /* Cria um mapa com controles de navegacao */
    map = new OpenLayers.Map({
        /* Adiciona controles de interacao com o usuario ao mapa */
        controls:[
            new OpenLayers.Control.LayerSwitcher(), /* Permite a troca de mapas base */
            new OpenLayers.Control.MousePosition(), /* Permite interacao com o mouse */
            new OpenLayers.Control.PanZoomBar(), /* Permite Zoom-in e Zoom-out */
            new OpenLayers.Control.Navigation() /* Permite barra de navegacao */
        ],
        /* Define a extancao maxima, ou area maxima permitida para o mapa, no caso todo o globo*/ 
        maxExtent: new OpenLayers.Bounds(-20037508.34,-20037508.34,
            20037508.34,20037508.34),
        maxResolution: 156543.0399,
        zoom: 14, /* Zoom maximo */
        units: 'm', /* Unidade de medida, metros*/
        
        /* Define o modelo de coordenadas do mapa, no caso, o sistema base usara o modelo de 
        cordenadas 'Spherical Mercator' */
        projection: new OpenLayers.Projection("EPSG:900913"), 
        
        /* Define o modelo de coordenadas de visualizacao, no caso, o sistema base usara o modelo de 
        cordenadas 'WGS84' */
        displayProjection: new OpenLayers.Projection("EPSG:4326"),
        
        /* Adiciona os mapas base ao mapa */ 
        layers: [
            osmBaseLayer
			/*, googlePhyMap, googleStreetMap, googleSatMap, googleHybMap */
        ]
    });                     			
	
	/**
     * Objeto GeoExt que encapsula o map OpenLayers em um Panel 
	 * ExtJS para facilitar a integracao com ExtJS 
	 */
    mapPanel = new GeoExt.MapPanel({
        map: map,                   
        center: new OpenLayers.LonLat(-47.89807,-22.00578).transform(
                    new OpenLayers.Projection("EPSG:4326"), 
                    map.getProjectionObject()),		
    });  
	
	/* Objeto que armazena o endereco dos servicos SOS */
    sensorClient = new SOS_Client({
        serviceURLs: SOS_SERVICES,
        map: map
    });
		
	/* Solicita ao serviço SOS as medicoes do sendor 'foi_3001'*/
    getObservationByFoi(storeSensor1, sensorClient, 'foi_3001', 
        OBSERVED_PROPERTY_WATER_LEVEL, 
        new Date(new Date() - (LONG_GRAPH_TIME_PERIOD*60*60000)), 
        new Date()); 
	
	
	/* Solicita ao serviço SOS as medicoes do sendor 'foi_4001'*/
    getObservationByFoi(storeSensor2, sensorClient, 'foi_4001', 
        OBSERVED_PROPERTY_WATER_LEVEL, 
        new Date(new Date() - (LONG_GRAPH_TIME_PERIOD*60*60000)), 
        new Date());
	
	/**
	 * adiciona controle para selecao dos sensores
	 */
	for(i = 0; i < sensorClient.services.length; i++){
        map.addControl(new OpenLayers.Control.SelectFeature(sensorClient.services[i].serviceLayer,
            {			
            scope: this, 
            onSelect: function(feature){
                selectedFeature = feature;
                onFeatureSelect(selectedFeature, 
                    OBSERVED_PROPERTY_WATER_LEVEL, 
                    sensorClient, sosStore, 
                    new Date(new Date() - (SHORT_GRAPH_TIME_PERIOD*60*60000)), 
                    new Date()); 
            },                                                    
            onBeforeSelect: function(){			
                sosStore.removeAll(); // Apaga os dados 
            },
            onUnselect: function(){			
                sosStore.removeAll(); // Apaga os dados 
            },
            autoActivate: true
	}))
	}   
	
    /**
     *Constroi arvore de camadas     
     */
    var layerRoot = new Ext.tree.TreeNode({
        text: "All Layers"
    });
    
	/**
	 * Monta a arvore de selecao de camadas
	 */
    layerRoot.appendChild(new GeoExt.tree.BaseLayerContainer({
        text: "Mapa Base",
        map: map,
        expanded: true
    }));
   
    var layerTree = new Ext.tree.TreePanel({
        title: "Mapa Base",
        root: layerRoot,
        rootVisible: false, 
        autoScroll: true,
        listeners: {             
            checkchange: function(node, checked) {    
                if (node.attributes.layer.isBaseLayer == false){
                    if (checked === true){
                        mapPanel.map.addLayer(node.attributes.layer);
                    }
                    else{                                           
                        mapPanel.map.removeLayer(
                            node.attributes.layer);
                    }
                }
            }            
        }        
    });
	
    /**
     * Definicao do risco media das ultimas N medicoes
     */			
    getRisk(riskStore, tendencyStore, storeSensor1, storeSensor2);
    
	/**
	 * Define a funcao chamada a um certo intervalo de tempo para atualizar os stores
	 */
	window.setInterval(function(){
		getRisk(riskStore, tendencyStore, storeSensor1, storeSensor2);
		update(sensorClient, sosStore, selectedFeature, storeSensor1, storeSensor2);
	}, UPDATE_INTERVAL);
	
    /**
     * Layout geral da aplicacao, todos os items da interface ficam
	 * organizados dentro dessa viewport.
	 *
	 * Para referencia acessar: http://dev.sencha.com/deploy/ext-3.4.0/examples/layout/complex.html
     */		
     viewport = new Ext.Viewport({
        layout: 'border', 		
        items: [{ 
			xtype: "panel",
			title: "Gr&aacute;ficos e imagens",
			ref: "east",
			region: "east",				
			collapsible: false,
			width: 500,
			layout: {
				type: "vbox",				
				align: "stretch"			
			},
			items: [
			{
				xtype: "panel",				
				title: "USP - Ultimas " + LONG_GRAPH_TIME_PERIOD + " horas",
				ref: "graph1Panel",
				flex: 1,
				layout: 'fit',
				items: [{
					xtype: 'linechart',
					store: storeSensor1,
					xField: 'Horario',					
					xAxis: new Ext.chart.CategoryAxis({
						title: 'Hora'
					}),
					yAxis: new Ext.chart.NumericAxis({
						title: 'Nivel(cm)',
						minimum: 0,
						maximum: 300,
						majorUnit: 50
					}),
					series:[{
						yField: 'Valor',
						style: {
							size: GRAPH_SIZE.small						
						}
					}],
					chartStyle: {                
						animationEnabled: false
					},
					extraStyle: {
						yAxis: {
							titleRotation: -90,							
							hideOverlappingLabels: true
						}
					}
				}]
			},
			{
				xtype: "panel",
				title: "KARTODROMO - Ultimas " + LONG_GRAPH_TIME_PERIOD + " horas",
				ref: "graph2Panel",
				flex: 1,
				layout: 'fit',
				items: [{
					xtype: 'linechart',
					store: storeSensor2,
					xField: 'Horario',					
					xAxis: new Ext.chart.CategoryAxis({
						title: 'Hora'
					}),
					yAxis: new Ext.chart.NumericAxis({
						title: 'Nivel(cm)',
						minimum: 0,
						maximum: 300,
						majorUnit: 50
					}),
					series:[{
						yField: 'Valor',						
						style: {							
							size: GRAPH_SIZE.small	
						}
					}],
					chartStyle: {                
						animationEnabled: false
					},
					extraStyle: {
						yAxis: {
							titleRotation: -90,
							hideOverlappingLabels: true
						}
					}
				}]	
			},
			{
				flex: 1,
				xtype: 'panel',
				layout: 'fit',
				items: [{
					xtype: "form",
					title: "Dados Hist&oacute;ricos",				
					ref: "form1Panel",					
					defaults: {width: 350},
					frame:true,        
					bodyStyle:'padding:5px 5px 0',					
					defaultType: 'datefield',					
					items: [{
						xtype: 'combo',
						id: 'foiCmb',
						fieldLabel: 'Sensor',
						hiddenName: 'ddi_sensor',
						emptyText: 'Selecione o sensor...',
						store: 
						  new Ext.data.SimpleStore({
							fields: ['foi','sensor'],
							data: [
							  ["foi_3001","USP"],["foi_4001","KARTODROMO"]
								  ]
						}), // end of Ext.data.SimpleStore
						displayField: 'sensor',
						valueField: 'foi',
						selectOnFocus: true,
						mode: 'local',
						typeAhead: true,
						editable: false,
						triggerAction: 'all',						
					},{						
						fieldLabel: 'Data inicial',
						editable: false,
						name: 'startdt',
						id: 'startdt',
						vtype: 'daterange',
						format: 'd/m/Y',
						endDateField: 'enddt' // id of the end date field
					},{						
						fieldLabel: 'Data final',
						editable: false,
						name: 'enddt',
						id: 'enddt',
						vtype: 'daterange',
						format: 'd/m/Y',
						startDateField: 'startdt' // id of the start date field
					}],
					buttons: [{
						text: 'Buscar',
						handler: function(){
						
							function parseDt(dt){
								var d = dt.substr(0, 2);
								var m = dt.substr(3, 2);
								var y = dt.substr(6, 4);
								
								return m +'/'+ d +'/'+ y;
							}											
							
							var startDate = new Date(parseDt(Ext.getCmp('startdt').value));												
							var endDate = new Date(parseDt(Ext.getCmp('enddt').value));
							
							startDate.setDate(startDate.getDate());
							endDate.setDate(endDate.getDate()+1);

							var selectedFoi = Ext.getCmp('foiCmb').value;							
							
							maskPage(true);	
							getObservationByFoi(sosHistoryStore, sensorClient, selectedFoi, OBSERVED_PROPERTY_WATER_LEVEL, startDate, endDate); 
							maskPage(false);	
						
							var win = new Ext.Window({								
								layout:'fit',
								width:840,
								height:480,
								closable : false,
								//plain: true,
								title: 'Dados Hist&oacute;ricos',								

								items: [{
									xtype: 'tabpanel',
									activeTab: 1,
									items: [{
										title: 'Tabela',
										layout: 'fit',
										items: [getObservationGrid(sosHistoryStore)]
									},{
										title: 'Grafico',
										layout: 'fit',
										items: [{
											xtype: 'panel',
											layout: 'fit',
											items: [{
												xtype: 'linechart',
												id: 'selectedHistorySensorChart',
												store: sosHistoryStore,
												xField: 'Horario',
												yField: 'Valor',
												xAxis: new Ext.chart.CategoryAxis({
													title: 'Hora'
												}),
												yAxis: new Ext.chart.NumericAxis({
													title: 'Nivel(cm)'
												}),
												chartStyle: {                
													animationEnabled: false
												},
												extraStyle: {
													yAxis: {
														titleRotation: -90,
														hideOverlappingLabels: true
													}
												}								
											}]					
										}]
									}]
								}],

								buttons: [{
									text: 'Fechar',
									handler: function(){
										sosHistoryStore.removeAll();
										win.destroy();
									}
								}]
							});							
							win.show(this);
						}
					}]
				}]				
			}]
        },    
        {
            region: 'west',
			collapsible: false,
            title: 'Controles',                
            width: 300,
			layout: {
				type: "vbox",				
				align: "stretch"
			},
			items: [{
				flex: 1,
				xtype: "panel",
				title: "Camadas",				
				layout: "fit",
				items: [{
					layout: {
						type: 'accordion',
						animate: true					
					},
					items: [
						layerTree
						/*,
						{
							title: "Opcao 1"
						},
						{
							title: "Opcao 2"
						}
						*/
					]
				}]								
			},
			{
				flex: 1,
				xtype: "panel",
				title: "An&aacute;lise de risco",
				layout: {				
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					flex: 1,
					xtype: 'panel',
					items: [{
						xtype: 'barchart',
						store: riskStore,
						yField: 'risk',
						yAxis: new Ext.chart.CategoryAxis({
							title: 'Risco'
						}),
						xAxis: new Ext.chart.NumericAxis({
							title: 'Nivel(cm)'
						}),					
						series: [{
							xField: 'veryHigh',
							displayName: 'Muito-Alto',
							style:{
								color: '#FE231F'
							}
						},{
							xField: 'high',
							displayName: 'Alto',
							style:{
								color: '#F39A1E'
							}
						},{
							xField: 'moderate',
							displayName: 'Moderado',
							style:{
								color: '#E4E629'
							}
						},{
							xField: 'low',
							displayName: 'Baixo',
							style:{
								color: '#AFE429'
							}
						},{
							xField: 'veryLow',
							displayName: 'Muito-Baixo',
							style:{
								color: '#76D90C'
							}
						}],
						chartStyle: {                
							animationEnabled: false
						},
						extraStyle: {
							yAxis: {
								titleRotation: -90,
								labelRotation: -90
							}
						}
					}]
				},{
					flex: 1,
					xtype: 'panel',
					title: 'Tend&ecirc;ncia de nivel',
					items: [{
						xtype: 'columnchart',
						store: tendencyStore,
						xField: 'tendency',
						xAxis: new Ext.chart.CategoryAxis({
							title: 'Tendencia'
						}),
						yAxis: new Ext.chart.NumericAxis({
							title: 'Nivel(cm)'
						}),					
						series: [{
							yField: 'value',
							displayName: 'Tendencia',
							style:{
								color: '#76D90C'
							}
						}],
						chartStyle: {                
							animationEnabled: false
						},
						extraStyle: {
							yAxis: {
								titleRotation: -90							
							}
						}
					}]
				}]
			}]	            			       
        },
		{
            region: "center", 						
            layout: {
                type: "vbox",								
				align: "stretch"
            },
            items: [{
				xtype: "panel",
				title: "Mapa",
				flex: 1,
				layout: "fit",
				items: mapPanel
			},{
				xtype: "panel",
				title: "Medi&ccedil;&otilde;es recentes - &Uacute;ltimas " + SHORT_GRAPH_TIME_PERIOD + " horas",
				flex: 0.48,
				layout: "fit",
				items: [{
					xtype: "tabpanel",				
					activeTab: 1,																			
					items: [{
						title: 'Tabela',
						layout: 'fit',
						items: [getObservationGrid(sosStore)]
					},{
						title : 'Grafico',
						layout: 'fit',
						items: [{
							xtype: 'panel',
							layout: 'fit',
							items: [{
								xtype: 'linechart',
								id: 'selectedSensorChart',
								store: sosStore,
								xField: 'Horario',
								yField: 'Valor',
								xAxis: new Ext.chart.CategoryAxis({
									title: 'Hora'
								}),
								yAxis: new Ext.chart.NumericAxis({
									title: 'Nivel(cm)'
								}),
								chartStyle: {                
									animationEnabled: false
								},
								extraStyle: {
									yAxis: {
										titleRotation: -90,
										hideOverlappingLabels: true
									}
								}								
							}]					
						}]
					}]
				}]								
			}]         
        }]
    });
	
	maskPage(false); 
});
